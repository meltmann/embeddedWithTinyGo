package main

// This is the most minimal blinky example and should run almost everywhere.

import (
	"machine"
	"time"
)

func main() {
	led := machine.IO26
	led.Configure(machine.PinConfig{Mode: machine.PinOutput})

	ledIsHigh := true

	for {
		ledIsHigh = !ledIsHigh

		switch ledIsHigh {
		case true:
			led.High()
		case false:
			led.Low()
		}

		time.Sleep(1 * time.Second)
	}
}
