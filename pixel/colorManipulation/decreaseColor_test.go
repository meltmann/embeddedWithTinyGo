package colorManipulation

import (
	"image/color"
	"testing"
)

type colorTestTable struct {
	start color.RGBA
	steps uint8
	expect color.RGBA
}

type channelTestTable struct {
	in color.RGBA
	expect channel
}

func TestHighestChannel(t *testing.T) {
	items := []channelTestTable{{
		in: color.RGBA{
			R: 255,
			G: 128,
			B: 64,
			A: 0,
		},
		expect: red,
	},
		{
			in: color.RGBA{
				R: 64,
				G: 128,
				B: 255,
				A: 0,
			},
			expect: blue,
		},
		{
			in: color.RGBA{
				R: 192,
				G: 255,
				B: 64,
				A: 0,
			},
			expect: green,
		},
		{
			in: color.RGBA{
				R: 64,
				G: 255,
				B: 192,
				A: 0,
			},
			expect: green,
		},
	}

	for row, table := range items {
		result := highestChannel(table.in)

		if result != table.expect {
			t.Errorf("Dude, why is row %#v %#v instead of %#v?", row, result, table.expect)
		}
	}
}

func TestDecrease_MainColor(t *testing.T) {
	table := []colorTestTable {
		{
			start: color.RGBA{
				R: 0,
				G: 255,
				B: 0,
				A: 0,
			},
			steps:  1,
			expect: color.RGBA{
				R: 0,
				G: 254,
				B: 0,
				A: 0,
			},
		},
		{
			start: color.RGBA{
				R: 0,
				G: 255,
				B: 0,
				A: 0,
			},
			steps:  128,
			expect: color.RGBA{
				R: 0,
				G: 127,
				B: 0,
				A: 0,
			},
		},
		{
			start: color.RGBA{
				R: 0,
				G: 255,
				B: 0,
				A: 0,
			},
			steps:  255,
			expect: color.RGBA{
				R: 0,
				G: 0,
				B: 0,
				A: 0,
			},
		},
		{
			start: color.RGBA{
				R: 255,
				G: 0,
				B: 0,
				A: 0,
			},
			steps:  64,
			expect: color.RGBA{
				R: 191,
				G: 0,
				B: 0,
				A: 0,
			},
		},
		{
			start: color.RGBA{
				R: 0,
				G: 0,
				B: 255,
				A: 0,
			},
			steps:  192,
			expect: color.RGBA{
				R: 0,
				G: 0,
				B: 63,
				A: 0,
			},
		},
	}

	for row, testSet := range table {
		result := Decrease(testSet.start, testSet.steps)

		if testSet.expect != result {
			t.Errorf("That didn't work for row %#v. Got  %#v; Want %#v", row, result, testSet.expect)
		}
	}
}

type iterationsTestTable struct {
	amount int
	numOfLeds int
	expectedOverall int
	expectedEach int
}

func TestIterationsOverLeds(t *testing.T) {
	var table = []iterationsTestTable{
		{
			amount: 25,
			numOfLeds: 5,
			expectedOverall: 25,
			expectedEach: 5,
		},
	}

	for row, line := range table {
		resultOverall, resultEach := interationsOverLeds(line.amount, line.numOfLeds)

		if resultOverall != line.expectedOverall && resultEach != line.expectedEach {
			t.Errorf("Issue at row %#v, got (%#v, %#v), want (%#v,%#v)", row, resultOverall, resultEach, line.expectedOverall, line.expectedEach)
		}
	}

}