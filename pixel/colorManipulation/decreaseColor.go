package colorManipulation

import "image/color"

type channel int8

const (
	red channel = iota
	green
	blue
	alpha
)

func Decrease(in color.RGBA, amountOfSteps uint8) color.RGBA {
	result := color.RGBA{}

	highChannel := highestChannel(in)

	switch highChannel {
	case red:
		result.R = in.R - amountOfSteps
	case green:
		result.G = in.G - amountOfSteps
	case blue:
		result.B = in.B - amountOfSteps

	case alpha:
	default:
		return result
	}

	return result
}

func highestChannel(in color.RGBA) channel {
	result := alpha

	var highest uint8 = 0

	if in.R > highest {
		highest = in.R
		result = red
	}
	if in.G > highest {
		highest = in.G
		result = green
	}
	if in.B > highest {
		highest = in.B
		result = blue
	}

	return result
}


func interationsOverLeds(amount int, numberOfLeds int) (overall int, each int) {
	return 1, 1
}
