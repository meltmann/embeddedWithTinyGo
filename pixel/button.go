package main

import (
	"image/color"
	"machine"
	"pixel/colorManipulation"
	"time"

	"tinygo.org/x/drivers/ws2812"
)

const (
	okLed  = machine.IO26
	errLed = machine.IO16
	stripe = machine.IO33
)

var red = color.RGBA{R: 255}

var leds = [5]color.RGBA{red, red, red, red, red}

func main() {
	errLed.Configure(machine.PinConfig{Mode: machine.PinOutput})
	okLed.Configure(machine.PinConfig{Mode: machine.PinOutput})
	stripe.Configure(machine.PinConfig{Mode: machine.PinOutput})

	ws := ws2812.New(stripe)

	var step uint8 = 0

	go statusBlink(okLed)

	for {
		for i := range leds {
			leds[i] = colorManipulation.Decrease(color.RGBA{B: 255}, step)
		}

		err := ws.WriteColors(leds[:])
		if err != nil {
			errLed.High()
		}

		time.Sleep(100 * time.Millisecond)

		if step == 255 {
			errLed.High()
			time.Sleep(2 * time.Second)
			errLed.Low()
		}

		step = step + 1
	}
}
func statusBlink(pin machine.Pin) {
	for {
		pin.High()
		time.Sleep(500 * time.Millisecond)

		pin.Low()
		time.Sleep(500 * time.Millisecond)
	}
}
